#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : os模块.py
@Author  : 神秘藏宝室
@Time    : 2020/2/12 22:47
店铺     : https://ilovemcu.taobao.com
"""
import os
# 获取操作系统类型
print(os.name)      # nt是windows系统，posix是linux系统，unix系统，Mac os X
# print(os.uname()) # windows下没用

print(os.environ)   # 获取环境变量
print('============')
print(os.environ.get("_OLD_VIRTUAL_PATH")) #获取某个软件的变量
print(os.curdir) #获取当前目录
print(os.getcwd()) # 获取当前工作所在目录
print(os.listdir()) # 获取当前文件的列表
# os.makedev('sunck') # 新建文件夹/目录
# os.rmdir('sunck')   # 删除目录
#获取文件属性
print(os.stat('os模块.py'))
#重命名
# print(os.rename())

# cmd指令 shell指令
# os.system("calc")
# 有些在os.path里面
# 当前绝对路径
print(os.path.abspath("./README.md"))

