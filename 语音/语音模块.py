#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : 语音模块.py
@Author  : 神秘藏宝室
@Time    : 2020/2/13 9:30
店铺     : https://ilovemcu.taobao.com
"""
from win32com.client import constants
import os
import win32com.client
import pythoncom
import win32api


class SpeechRecognition:
    def __init__(self, wordsToAdd):
        self.speaker = win32com.client.Dispatch("SAPI.SpVoice")
        self.listener = win32com.client.Dispatch("SAPI.SpSharedRecognizer")
        self.context = self.listener.CreateRecoContext()
        self.grammar = self.context.CreateGrammar()
        self.grammar.DictationSetState(0)
        self.wordsRule = self.grammar.Rules.Add("wordsRule", constants.SRATopLevel + constants.SRADynamic, 0)
        self.wordsRule.Clear()
        [self.wordsRule.InitialState.AddWordTransition(None, word) for word in wordsToAdd]
        self.grammar.Rules.Commit()
        self.grammar.CmdSetRuleState("wordsRule", 1)
        self.grammar.Rules.Commit()
        self.eventHandler = ContextEvents(self.context)
        self.say("Started successfully")

    def say(self, phrase):
        self.speaker.Speak(phrase)


class ContextEvents(win32com.client.getevents("SAPI.SpSharedRecoContext")):
    def OnRecognition(self, StreamNumber, StreamPosition, RecognitionType, Result):
        newResult = win32com.client.Dispatch(Result)
        print("说：", newResult.PhraseInfo.GetText())
        s = newResult.PhraseInfo.GetText()

        if s == "记事本":
            os.system("notepad")
        elif s == '我好无聊':
            path = r"C:\Program Files (x86)\Tencent\QQ\Bin\QQScLauncher.exe"
            win32api.ShellExecute(0, 'open', path, '', '', 1)
            speak = win32com.client.Dispatch('SAPI.SPVOICE')
            speak.Speak('主人，qq找个人聊天吧')

if __name__ == '__main__':
    wordsToAdd = ["关机", "取消关机", "记事本", "画图板", "写字板", "设置", "关闭记事本", '我好无聊']
    speechReco = SpeechRecognition(wordsToAdd)

    while True:
        pythoncom.PumpWaitingMessages()
