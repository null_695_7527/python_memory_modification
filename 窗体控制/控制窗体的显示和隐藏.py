#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : 控制窗体的显示和隐藏.py
@Author  : 神秘藏宝室
@Time    : 2020/2/12 23:33
店铺     : https://ilovemcu.taobao.com
"""

'''
pip install pywin32 -i https://pypi.doubanio.com/simple
'''
import win32con
import win32gui
import time

# 找出窗体的编号
# 植物大战僵尸的窗体
pzWin = win32gui.FindWindow('MainWindow', 'Plants vs. Zombies')
print(pzWin)
# 显示和隐藏窗体
while True:
    win32gui.ShowWindow(pzWin, win32con.SW_SHOW)
    time.sleep(2)
    win32gui.ShowWindow(pzWin, win32con.SW_HIDE)
    time.sleep(2)
    print('loop')
