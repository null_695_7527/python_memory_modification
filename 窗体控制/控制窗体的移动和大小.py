#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : 控制窗体的移动和大小.py
@Author  : 神秘藏宝室
@Time    : 2020/2/13 8:51
店铺     : https://ilovemcu.taobao.com
"""
import win32con
import win32gui
import random

# QQ的窗体
pzWin = win32gui.FindWindow('TXGuiFoundation', 'QQ')
print(pzWin)
# 设置大致位置
# 参数1：控制的参数
# 参数2：大概方位
# 参数3：x
# 参数4：y
# 参数5：宽度
# 参数6：长度

# win32con.HWND_TOPMOST 最上面

while True:
    x = random.randrange(900)
    y = random.randrange(400)
    win32gui.SetWindowPos(pzWin, win32con.HWND_TOPMOST, x, y, 600, 500, win32con.SWP_SHOWWINDOW)
