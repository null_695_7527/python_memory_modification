# python内存修改

#### 介绍
python窗口控制内存修改学习
想学习下如何操作内存，比如做个游戏的修改器什么的
学习pygame基础
#### 学习参考视频
https://www.ixigua.com/home/2546109855314893/video/
https://www.bilibili.com/video/av19574503?p=13

#### 软件架构
窗体控制：测试代码

#### 使用工具
spylite24是spy++,用于获取窗体参数
cheatEngine可以作为内存查询修改软件，游戏修改器

#### 安装教程
我的python3.7版本直接能运行pywin32的库，没单独安装
```
pip install pywin32 -i https://pypi.doubanio.com/simple
pip install pygame -i https://pypi.doubanio.com/simple
```

