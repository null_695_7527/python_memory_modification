#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : startPygame.py
@Author  : 神秘藏宝室
@Time    : 2020/2/14 18:55
店铺     : https://ilovemcu.taobao.com
"""
'''
第一次装pygame不支持自动补全代码，网上查询重装就可以了。
pip install pygame -i https://pypi.tuna.tsinghua.edu.cn/simple/
'''

# pygame最小环境
import sys, pygame

# 初始化
pygame.init()
scream = pygame.display.set_mode((600, 400))
pygame.display.set_caption('pygame测试')

# 循环
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        pygame.display.update()
