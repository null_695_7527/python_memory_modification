#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : pygame绘图.py
@Author  : 神秘藏宝室
@Time    : 2020/2/15 9:40
店铺     : https://ilovemcu.taobao.com
"""

# pygame最小环境
import sys, pygame
import pygame.freetype

# 初始化
size = width, height = 800, 600
pygame.init()
screen = pygame.display.set_mode(size, pygame.RESIZABLE)
pygame.display.set_caption('pygame测试绘制')


GOLD = 255, 251, 0
RED = pygame.Color('red')

f1 = pygame.freetype.Font(r"C:\Windows\Fonts\msyh.ttc", 36)
f1surf, f1rect = f1.render('我真帅', fgcolor=GOLD, bgcolor=RED, size=35)

def myDraw():
    r1rect = pygame.draw.rect(screen, GOLD, (100, 100, 200, 100), 5)
    r2rect = pygame.draw.circle(screen, RED, (300, 300), 50, 1)
    r3rect = pygame.draw.circle(screen, RED, (500, 300), 50, 0)

    # f1rect = f1.render_to(screen, (200, 160), '我真帅', fgcolor=GOLD, bgcolor=RED, size=35)



myDraw()
# 循环
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.VIDEORESIZE:  # 重新修正屏幕尺寸
            print('resize')
            size = width, height = event.size[0], event.size[1]
            screen = pygame.display.set_mode(size, pygame.RESIZABLE)
            # myDraw()
    screen.blit(f1surf,(200,160))
    pygame.display.update()
