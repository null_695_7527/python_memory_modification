#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : 壁球游戏文字型.py
@Author  : 神秘藏宝室
@Time    : 2020/2/15 10:23
店铺     : https://ilovemcu.taobao.com
"""

# pygame最小环境
import sys, pygame
import pygame.freetype

# 初始化
pygame.init()

speed = [1, 1]
BLACK = 0, 0, 0
# vInfo = pygame.display.Info()
# size = width, height = vInfo.current_w, vInfo.current_h
size = width, height = 600, 400
screen = pygame.display.set_mode(size, pygame.RESIZABLE)
pygame.display.set_caption('碰壁小球游戏')
# 设置图标
icon = pygame.image.load("title.png")
pygame.display.set_icon(icon)
# 定义小球
ball = pygame.image.load("ball.png")
# 获得覆盖球的方形对象
ballrect = ball.get_rect()

GOLD = 255, 251, 0
RED = pygame.Color('red')
f1 = pygame.freetype.Font(r"C:\Windows\Fonts\msyh.ttc", 36)
f1surf, f1rect = f1.render('我真帅', fgcolor=GOLD, size=35)

# fps
fps = 300  # 定义fps
fclock = pygame.time.Clock()

bgColor = pygame.Color('black')
pos = [100,200]

def RGBChannel(a):
    return 0 if a < 0 else (255 if a > 255 else int(a))


# 循环
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                print("L", speed)
                speed[0] = speed[0] if speed[0] == 0 else (abs(speed[0]) - 1) * int(speed[0] / abs(speed[0]))
            if event.key == pygame.K_RIGHT:
                print("R", speed)
                speed[0] = speed[0] + 1 if speed[0] > 0 else speed[0] - 1
            if event.key == pygame.K_UP:
                print("U", speed)
                speed[1] = speed[1] if speed[1] == 0 else (abs(speed[1]) - 1) * int(speed[1] / abs(speed[1]))
            if event.key == pygame.K_DOWN:
                print("D", speed)
                speed[1] = speed[1] + 1 if speed[1] > 0 else speed[1] - 1
        elif event.type == pygame.VIDEORESIZE:  # 重新修正屏幕尺寸
            size = width, height = event.size[0], event.size[1]
            screen = pygame.display.set_mode(size, pygame.RESIZABLE)
        elif event.type == pygame.MOUSEBUTTONDOWN:
            # pos是位置，button是1是左键，3是右键
            print(event.pos, event.button)

    if pos[0] < 0 or pos[0] + f1rect.width > width:
        speed[0] = -speed[0]
    if pos[1] < 0 or pos[1] + f1rect.height > height:
        speed[1] = -speed[1]

    bgColor.r = RGBChannel(pos[0] * 255 / width)
    bgColor.g = RGBChannel(pos[1] * 255 / height)
    bgColor.b = RGBChannel(min(speed[0], speed[1]) * 255 / max(speed[0], speed[1], 1))
    screen.fill(bgColor)

    pos[0] = pos[0] + speed[0]
    pos[1] = pos[1] + speed[1]

    # screen.blit(ball, ballrect)
    screen.blit(f1surf,(pos[0],pos[1]))

    pygame.display.update()
    fclock.tick(fps)
