#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : 植物大战僵尸阳光数基地址内存修改.py
@Author  : 神秘藏宝室
@Time    : 2020/2/12 20:37
店铺     : https://ilovemcu.taobao.com
"""

# 进程模块
import win32process
# 系统
import win32con
import win32gui
import win32api
import ctypes

# 管理员身份打开进程
PROCESS_ALL_ACCESS = (0x000F0000 | 0x00100000 | 0xfff)

# 找窗体
# 植物大战僵尸的窗体
pzWin = win32gui.FindWindow('MainWindow', 'Plants vs. Zombies')
print(pzWin)
# 根据窗体找到进程号
hid, pid = win32process.GetWindowThreadProcessId(pzWin)
print(hid, pid)
# 以最高权限打开进程
p = win32api.OpenProcess(PROCESS_ALL_ACCESS, False, pid)
print(p)
# 获取数据
data = ctypes.c_long()
# 加载内核模块
md = ctypes.windll.LoadLibrary('C:\\Windows\\System32\\kernel32.dll')
# # 读取内存
# md.ReadProcessMemory(int(p),0x316E6B18, ctypes.byref(data), 4, None)
# print("data:", data)
# #新值
# newData = ctypes.c_long(10000)
# #修改
# md.WriteProcessMemory(int(p), 0x316E6B18, ctypes.byref(newData), 4, None)

'''
# 直接去找他的地址
add eax,[edx+00005578]
其中eax就是存的阳光50
eax= 00000032
edx = 2FBD3548

mov ecx,[eax+00000868]
02799FA8

'''
# 读取内存
md.ReadProcessMemory(int(p), 0x00755e0c, ctypes.byref(data), 4, None)
print("data:", data, hex(data.value))
data2 = ctypes.c_long()
md.ReadProcessMemory(int(p), data.value + 0x868, ctypes.byref(data2), 4, None)
print("data2:", data2, hex(data2.value))
data3 = ctypes.c_long()
md.ReadProcessMemory(int(p), data2.value + 0x5578, ctypes.byref(data3), 4, None)
print("data3:", data3, hex(data3.value))
#修改
newInput = input('请输入阳光数字')
newData = ctypes.c_long(int(newInput))
md.WriteProcessMemory(int(p), data2.value + 0x5578, ctypes.byref(newData), 4, None)
md.ReadProcessMemory(int(p), data2.value + 0x5578, ctypes.byref(data3), 4, None)
print("data3:", data3, data3.value)
